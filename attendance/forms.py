from django import forms

COURSE_OPTIONS = [
    ('BCA', 'BCA'),
    ('MCA', 'MCA'),
    ('BTECH', 'BTECH'),
]

YEAR_OPTIONS = [
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
]

SECTION_OPTIONS = [
    ('A', 'A'),
    ('B', 'B'),
    ('C', 'C'),
]


class MakeAttendance(forms.Form):
    courses = forms.ChoiceField(label='Select Course', choices=COURSE_OPTIONS,
                                widget=forms.Select(attrs={'class': 'dropdown-item'}))
    year = forms.ChoiceField(label='Select Year', choices=YEAR_OPTIONS,
                             widget=forms.Select(attrs={'class': 'dropdown-item'}))
    section = forms.ChoiceField(label='Select Section', choices=SECTION_OPTIONS,
                                widget=forms.Select(attrs={'class': 'dropdown-item'}))
