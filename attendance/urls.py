from django.urls import path
from .views import Attendance

urlpatterns = [
    path('/', Attendance.home, name='attendance_home'),
    path("/generate/", Attendance.generate_qr, name="generate_qr"),
]
