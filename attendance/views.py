import os
from django.shortcuts import render, redirect
from core import custom_print
from .forms import MakeAttendance
from users.models import UserProfile
import qrcode
import base64
import core

# Create your views here.

SAVE_PATH = core.BASE_DIR + "\\finalproject\static\img\qr\\"


class Attendance:
    @staticmethod
    def home(request):
        form = MakeAttendance()
        return redirect(request, 'attendance/get_attendance.html', {'form': form})

    @staticmethod
    def generate_qr(request):
        if request.method == "POST":
            form = MakeAttendance(request.POST)
            if form.is_valid():
                course = form.cleaned_data.get("courses")
                section = form.cleaned_data.get("section")
                year = form.cleaned_data.get("year")
                b64payload = course+section+str(year)
                b64payload = b64payload.encode()
                unique_id = base64.b64encode(b64payload)
                userdbobject = UserProfile.objects.all()
                userdbobject.filter(course=course, section=section, year=year).update(qrcode=unique_id.decode('utf-8'))
                qr = qrcode.QRCode(version=1,
                                   error_correction=qrcode.constants.ERROR_CORRECT_L,
                                   box_size=10,
                                   border=4,
                                   )
                qr.add_data(unique_id)
                qr.make(fit=True)
                img = qr.make_image()
                with open(str(base64.b64decode(unique_id)) + '.jpg', 'wb') as f:
                    custom_print("GENERATED QR CODE BRO!")
                    img.save(f)
                os.rename(str(base64.b64decode(unique_id))+'.jpg', SAVE_PATH+unique_id.decode('utf-8')+'.jpg')
                return redirect('/users/dashboard')

        return redirect('/users/dashboard')
