import os
from colorama import init, Fore


init(autoreset=True)


def custom_print(what):
    print(Fore.RED + what)


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
