from django.shortcuts import render
from django.http import HttpResponse


def home(request):
    return render(request, "index/home.html")


def robots(request):
    with open("robots.txt", "r") as robots_txt:
        return HttpResponse(robots_txt.read())
