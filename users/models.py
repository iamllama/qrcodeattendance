from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    section = models.CharField(max_length=255, blank=True)
    year = models.IntegerField(null=True, blank=True)
    course = models.CharField(max_length=255, blank=True)
    qrcode = models.CharField(max_length=255, blank=True)
    present = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username


# class StudentAttendance(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE)
#     qrcode =
