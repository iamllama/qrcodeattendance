from django.urls import path
from .views import Users

urlpatterns = [
    path("/", Users.login_request, name="user_login"),
    path(r"register$", Users.register, name="register"),
    path("/logout/", Users.logout_request, name="user_logout"),
    path("/dashboard/", Users.dashboard, name="user_dashboard"),
]
