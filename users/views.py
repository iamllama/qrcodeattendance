from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from attendance.forms import MakeAttendance
from core import custom_print
from .models import UserProfile

# Create your views here.

form = AuthenticationForm()


class Users:
    @staticmethod
    def register(request):
        pass

    @staticmethod
    def logout_request(request):
        logout(request)
        return redirect('/users/')

    @staticmethod
    def login_request(request):
        form = AuthenticationForm()
        custom_print("login request started")
        if request.method == "POST":
            form = AuthenticationForm(request, data=request.POST)
            if form.is_valid():
                username = form.cleaned_data.get('username')
                password = form.cleaned_data.get('password')
                user = authenticate(username=username, password=password)
                if user is not None:
                    login(request, user)
                    messages.info(request, f"You're logged in as {username}")
                    return redirect('/users/dashboard')
                else:
                    return render(request, "users/login.html",
                                  {"message": "Username or Password incorrect (user doesn't exist)", 'form': form})
            else:
                return render(request, "users/login.html", {"message": "Username or Password incorrect ", 'form': form})
        return render(request, "users/login.html", {'form': form})

    @staticmethod
    def dashboard(request):
        if request.user.is_authenticated:
            form = MakeAttendance()
            userprofile_db_object = UserProfile.objects.all().filter(user__username=request.user.username)
            print(userprofile_db_object)
            return render(request, 'users/dashboard.html',
                          {'user': request.user, 'form': form, 'userprofile_data': userprofile_db_object})
        else:
            return redirect('/users/', {"message": "You must login first!"})
